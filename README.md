# Open Foundry React Client
In Development. A React Client that interfaces with the [OpenFoundry API](https://gitlab.com/koeng/openfoundry-js).

## View Client Demo
Demo is visible at [biohacking.services](https://biohacking.services).

## View API Demo
The [OpenFoundry API](https://gitlab.com/koeng/openfoundry-js) Demo is visible at [api.biohacking.services](https://api.biohacking.services).

## Install
In your terminal:
```
git clone https://gitlab.com/koeng/openfoundry-js-client.git && cd openfoundry-js-client
```

### Install Modules
```
npm install
```

### Run
```
npm start
```
