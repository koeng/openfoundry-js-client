import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import LandingPage from './pages/LandingPage';
import LoginPage from './pages/LoginPage';
import SignupPage from './pages/SignupPage';
import ProfilePage from './pages/ProfilePage';

import SequencePage from './pages/SequencePage';

import BuildPage from './pages/BuildPage';
import BuildGenes from './components/BuildGenes';
import BuildSpecificGenes from './components/BuildSpecificGenes';

import TransformPage from './pages/TransformPage';
import TransformPlate from './components/TransformPlate';
import TransformPick from './components/TransformPick';
import TransformConsolidate from './components/TransformConsolidate';

import WorldList from './components/WorldList';
import WorldProfile from './components/WorldProfile';
import WorldNew from './components/WorldNew';
import WorldEdit from './components/WorldEdit';
import WorldDelete from './components/WorldDelete';

import FreezerList from './components/FreezerList';
import FreezerProfile from './components/FreezerProfile';
import FreezerNew from './components/FreezerNew';
import FreezerNewPlate from './components/FreezerNewPlate';
import FreezerEdit from './components/FreezerEdit';
import FreezerDelete from './components/FreezerDelete';

import PlateList from './components/PlateList';
import PlateProfile from './components/PlateProfile';
import PlateNew from './components/PlateNew';
import PlateEdit from './components/PlateEdit';
import PlateDelete from './components/PlateDelete';

import WellList from './components/WellList';
import WellProfile from './components/WellProfile';
import WellNew from './components/WellNew';
import WellEdit from './components/WellEdit';
import WellDelete from './components/WellDelete';

import DnaSampleList from './components/DnaSampleList';
import DnaSampleProfile from './components/DnaSampleProfile';
import DnaSampleNew from './components/DnaSampleNew';
import DnaSampleEdit from './components/DnaSampleEdit';
import DnaSampleDelete from './components/DnaSampleDelete';

import OrganismSampleList from './components/OrganismSampleList';
import OrganismSampleProfile from './components/OrganismSampleProfile';
import OrganismSampleNew from './components/OrganismSampleNew';
import OrganismSampleEdit from './components/OrganismSampleEdit';
import OrganismSampleDelete from './components/OrganismSampleDelete';

import SampleList from './components/SampleList';
import SampleProfile from './components/SampleProfile';
import SampleNew from './components/SampleNew';
import SampleEdit from './components/SampleEdit';
import SampleDelete from './components/SampleDelete';

class Routes extends Component {
  render() {
    const landingPage = () => {
      return ( <LandingPage {...this.props} /> );
    };
    const loginPage = () => {
      return ( <LoginPage {...this.props} /> );
    };
    const signupPage = () => {
      return ( <SignupPage {...this.props} /> );
    };
    const profilePage = () => {
      return ( <ProfilePage {...this.props} /> );
    };
    const sequencePage = () => {
      return ( <SequencePage {...this.props} /> );
    };
    const buildPage = () => {
      return ( <BuildPage {...this.props} /> );
    };
    const buildGenes = () => {
      return ( <BuildGenes {...this.props} /> );
    };
    const buildSpecificGenes = () => {
      return ( <BuildSpecificGenes {...this.props} /> );
    };
    const transformPage = () => {
      return ( <TransformPage {...this.props} /> );
    };
    const transformPlate = () => {
      return ( <TransformPlate {...this.props} /> );
    };
    const transformPick = () => {
      return ( <TransformPick {...this.props} /> );
    };
    const transformConsolidate = () => {
      return ( <TransformConsolidate {...this.props} /> );
    };
    
    const worldList = () => {
      return ( <WorldList {...this.props} /> );
    };
    const worldProfile = (match) => {
      return ( <WorldProfile {...this.props} match={match.match}/> );
    };
    const worldNew = () => {
      return ( <WorldNew {...this.props}/> );
    };
    const worldEdit = (match) => {
      return ( <WorldEdit {...this.props} match={match.match}/> );
    };
    const worldDelete = (match) => {
      return ( <WorldDelete {...this.props} match={match.match}/> );
    };

    const freezerList = () => {
      return ( <FreezerList {...this.props} /> );
    };
    const freezerProfile = (match) => {
      return ( <FreezerProfile {...this.props} match={match.match}/> );
    };
    const freezerNew = () => {
      return ( <FreezerNew {...this.props}/> );
    };
    const freezerNewPlate = (match) => {
      return ( <FreezerNewPlate {...this.props} match={match.match}/> );
    };
    const freezerEdit = (match) => {
      return ( <FreezerEdit {...this.props} match={match.match}/> );
    };
    const freezerDelete = (match) => {
      return ( <FreezerDelete {...this.props} match={match.match}/> );
    };

    const plateList = () => {
      return ( <PlateList {...this.props} /> );
    };
    const plateProfile = (match) => {
      return ( <PlateProfile {...this.props} match={match.match}/> );
    };
    const plateNew = () => {
      return ( <PlateNew {...this.props} /> );
    };
    const plateEdit = (match) => {
      return ( <PlateEdit {...this.props} match={match.match}/> );
    };
    const plateDelete = (match) => {
      return ( <PlateDelete {...this.props} match={match.match}/> );
    };

    const wellList = () => {
      return ( <WellList {...this.props} /> );
    };
    const wellNew = () => {
      return ( <WellNew {...this.props} /> );
    };
    const wellEdit = (match) => {
      return ( <WellEdit {...this.props} match={match.match}/> );
    };
    const wellProfile = (match) => {
      return ( <WellProfile {...this.props} match={match.match}/> );
    };
    const wellDelete = (match) => {
      return ( <WellDelete {...this.props} match={match.match}/> );
    };

    const dnaSampleList = () => {
      return ( <DnaSampleList {...this.props} /> );
    };
    const dnaSampleNew = () => {
      return ( <DnaSampleNew {...this.props} /> );
    };
    const dnaSampleEdit = (match) => {
      return ( <DnaSampleEdit {...this.props} match={match.match}/> );
    };
    const dnaSampleProfile = (match) => {
      return ( <DnaSampleProfile {...this.props} match={match.match}/> );
    };
    const dnaSampleDelete = (match) => {
      return ( <DnaSampleDelete {...this.props} match={match.match}/> );
    };

    const organismSampleList = () => {
      return ( <OrganismSampleList {...this.props} /> );
    };
    const organismSampleNew = () => {
      return ( <OrganismSampleNew {...this.props} /> );
    };
    const organismSampleEdit = (match) => {
      return ( <OrganismSampleEdit {...this.props} match={match.match}/> );
    };
    const organismSampleProfile = (match) => {
      return ( <OrganismSampleProfile {...this.props} match={match.match}/> );
    };
    const organismSampleDelete = (match) => {
      return ( <OrganismSampleDelete {...this.props} match={match.match}/> );
    };

    const sampleList = () => {
      return ( <SampleList {...this.props} /> );
    };
    const sampleNew = () => {
      return ( <SampleNew {...this.props} /> );
    };
    const sampleEdit = (match) => {
      return ( <SampleEdit {...this.props} match={match.match}/> );
    };
    const sampleProfile = (match) => {
      return ( <SampleProfile {...this.props} match={match.match}/> );
    };
    const sampleDelete = (match) => {
      return ( <SampleDelete {...this.props} match={match.match}/> );
    };

    return (
      <main>

        <Switch>
          <Route exact path='/' render={landingPage} />
          <Route exact path='/login' render={loginPage} />
          <Route exact path='/signup' render={signupPage} />
          <Route exact path='/profile' render={profilePage} />
          <Route exact path='/sequence' render={sequencePage} />
          <Route exact path='/build/genes' render={buildGenes} />
          <Route exact path='/build/specific-genes' render={buildSpecificGenes} />
          <Route exact path='/build' render={buildPage} />
          <Route exact path='/transform/plate' render={transformPlate} />
          <Route exact path='/transform/pick' render={transformPick} />
          <Route exact path='/transform/consolidate' render={transformConsolidate} />
          <Route exact path='/transform' render={transformPage} />
        </Switch>

        <Switch>
          <Route exact path='/worlds/new' render={worldNew} />
          <Route path='/worlds/:worldId/edit' render={worldEdit} />
          <Route path='/worlds/:worldId/delete' render={worldDelete} />
          <Route path='/worlds/:worldId' render={worldProfile} />
          <Route exact path='/worlds' render={worldList} />
        </Switch>

        <Switch>
          <Route exact path='/freezers/new' render={freezerNew} />
          <Route path='/freezers/:freezerId/new-plate' render={freezerNewPlate} />
          <Route path='/freezers/:freezerId/edit' render={freezerEdit} />
          <Route path='/freezers/:freezerId/delete' render={freezerDelete} />
          <Route path='/freezers/:freezerId' render={freezerProfile} />
          <Route exact path='/freezers' render={freezerList} />
        </Switch>

        <Switch>
          <Route exact path='/plates/new' render={plateNew} />
          <Route path='/plates/:plateId/edit' render={plateEdit} />
          <Route path='/plates/:plateId/delete' render={plateDelete} />
          <Route path='/plates/:plateId' render={plateProfile} />
          <Route exact path='/plates' render={plateList} />
        </Switch>

        <Switch>
          <Route exact path='/wells/new' render={wellNew} />
          <Route path='/wells/:wellId/edit' render={wellEdit} />
          <Route path='/wells/:wellId/delete' render={wellDelete} />
          <Route path='/wells/:wellId' render={wellProfile} />
          <Route exact path='/wells' render={wellList} />
        </Switch>
        
        <Switch>
          <Route exact path='/dna-samples/new' render={dnaSampleNew} />
          <Route path='/dna-samples/:sampleId/edit' render={dnaSampleEdit} />
          <Route path='/dna-samples/:sampleId/delete' render={dnaSampleDelete} />
          <Route path='/dna-samples/:sampleId' render={dnaSampleProfile} />
          <Route exact path='/dna-samples' render={dnaSampleList} />
        </Switch>

        <Switch>
          <Route exact path='/organism-samples/new' render={organismSampleNew} />
          <Route path='/organism-samples/:sampleId/edit' render={organismSampleEdit} />
          <Route path='/organism-samples/:sampleId/delete' render={organismSampleDelete} />
          <Route path='/organism-samples/:sampleId' render={organismSampleProfile} />
          <Route exact path='/organism-samples' render={organismSampleList} />
        </Switch>

        <Switch>
          <Route exact path='/samples/new' render={sampleNew} />
          <Route path='/samples/:sampleId/edit' render={sampleEdit} />
          <Route path='/samples/:sampleId/delete' render={sampleDelete} />
          <Route path='/samples/:sampleId' render={sampleProfile} />
          <Route exact path='/samples' render={sampleList} />
        </Switch>

      </main>
    );
  }
}

export default Routes;