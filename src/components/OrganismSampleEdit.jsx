import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class OrganismSampleEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      sampleId: "",
      sample: {
        parentId: "",
        name: "",
        description: "",
        provenance: "",
        mta: ""
      }
    };
    this.getSample = this.getSample.bind(this);
    this.updateSample = this.updateSample.bind(this);
    this.submitEditSample = this.submitEditSample.bind(this);
    this.handleNewSampleFormSubmit = this.handleNewSampleFormSubmit.bind(this);
  }

  getSample() {
    const sampleId = this.props.match.params.sampleId;
    axios.get(`https://api.biohacking.services/organism-samples/${sampleId}`)
    .then(res => {     
      this.setState({
        sample: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }


  updateSample(e) {
    const field = e.target.name;
    let sample = this.state.sample;
    sample[field] = e.target.value;
    this.setState({
      sample
    });    
  }

  submitEditSample(sample) {
    const sampleId = this.props.match.params.sampleId;
    if(sample.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post(`https://api.biohacking.services/organism-samples/${sampleId}/edit`, sample, config)
      .then(res => {       
        this.setState({
          redirect: true
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ sample });
      });      
    }  
  }

  handleNewSampleFormSubmit(e) {
    e.preventDefault();
    let sample = this.state.sample;
    this.submitEditSample(sample);
  }

  componentDidMount() {
    this.getSample();
  }

  render() {
    const sampleId = this.props.match.params.sampleId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/organism-samples/${sampleId}`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.sample).length > 0) ? (
                  <h4 className="card-title mb-0">Edit {this.state.sample.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Sample...</h4>
                )}  
              </div>
              {(Object.keys(this.state.sample).length > 0) ? (
                <div>
                  <div className="card-body">
                    <form onSubmit={this.handleNewSampleFormSubmit}>
                      <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input 
                          name="name"
                          className="form-control"
                          value={this.state.sample.name}
                          onChange={this.updateSample}
                          placeholder="Sample Name"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <input 
                          name="description"
                          className="form-control"
                          value={this.state.sample.description}
                          onChange={this.updateSample}
                          placeholder="A short description of the Sample."
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="provenance">Provenance</label>
                        <textarea 
                          name="provenance"
                          className="form-control"
                          value={this.state.sample.provenance}
                          onChange={this.updateSample}
                          placeholder="Sample Provenance."
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="mta">MTA</label>
                        <select 
                          name="mta"
                          className="form-control"
                          value={this.state.sample.mta}
                          onChange={this.updateSample}
                        >
                          <option>Select One</option>
                          <option value="OpenMTA">OpenMTA</option>
                          <option value="UBMTA">UBMTA</option>
                          <option value="Limbo">Limbo</option>
                        </select>
                      </div>

                      <div className="form-group text-center">
                        <div className="btn-group" role="group">
                          <Link to={`/organism-samples/${sampleId}`} className="btn btn-secondary mt-3">Back</Link>
                          <Link to={`/organism-samples/${sampleId}/delete`} className="btn btn-danger mt-3">Delete</Link>
                          <button type="submit" className="btn btn-success mt-3">Save Changes</button>
                        </div>
                      </div>                    
                                         
                    </form>    

                  </div>
                </div>
              ) : null }  
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default OrganismSampleEdit;