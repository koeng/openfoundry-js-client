import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class WellProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      wellId: "",
      plate: {},
      well: {},
      dnaSamples: [],
      organismSamples: [],
      samples: []
    };
    this.getWell = this.getWell.bind(this);
  }

  getWell() {
    const wellId = this.props.match.params.wellId;
    axios.get(`https://api.biohacking.services/wells/${wellId}`)
    .then(res => {
      //console.log(res.data);     
      this.setState({
        well: res.data.data,
        plate: res.data.data.parentId,
        dnaSamples: res.data.dnaSamples,
        organismSamples: res.data.organismSamples,
        samples: res.data.samples
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  componentDidMount() {
    this.getWell();
  }

  render() {
    const dnaSamples = this.state.dnaSamples.map((sample, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/dna-samples/${sample._id}`}
        >
          {sample.name}
        </Link>
      )
    });
    const organismSamples = this.state.organismSamples.map((sample, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/organism-samples/${sample._id}`}
        >
          {sample.name}
        </Link>
      )
    });
    const samples = this.state.samples.map((sample, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/samples/${sample._id}`}
        >
          {sample.name}
        </Link>
      )
    });
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">

            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.well).length > 0) ? (
                  <h4 className="card-title mb-0">{this.state.well.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Well...</h4>
                )}  
              </div>
              {(Object.keys(this.state.well).length > 0) ? (
                <div>
                  <div className="card-body">

                    <p className="card-text">
                      { this.state.well.description || "No description provided." }<br/>
                      Volume: {this.state.well.volume}
                    </p>

                  </div>
                  <ul className="list-group list-group-flush">
                  <Link 
                      className="list-group-item list-group-item-action bg-light"
                      to={`/plates/${this.state.plate._id}`}
                    >Plate - {this.state.plate.name}</Link> 
                    <Link 
                      className="list-group-item list-group-item-action bg-secondary text-light"
                      to="/wells"
                    >Well List</Link>                    
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-primary text-light"
                        to={`/wells/${this.state.well._id}/edit`}
                      >Edit Well</Link>
                    ) : null }
                  </ul>
                </div>
              ) : null }  
            </div>

            <div className="card mt-3">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.well).length > 0) ? (
                  <h4 className="card-title mb-0">DNA Samples In {this.state.well.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Well DNA Samples...</h4>
                )} 
              </div> 
              {(this.state.dnaSamples.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of DNA sample nodes inside {this.state.well.name}:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    <Link to="/dna-samples/new" className="list-group-item list-group-item-action bg-success text-light">Add New DNA Sample</Link>
                    {dnaSamples}
                  </ul>
                </div>  
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    There are currently no DNA samples inside {this.state.well.name || 'this Well'}.
                    &nbsp;<Link to={`/dna-samples/new`}>Add New DNA Sample</Link>.
                  </p>
                </div>
              )}
            </div>   

            <div className="card mt-3">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.well).length > 0) ? (
                  <h4 className="card-title mb-0">Organism Samples In {this.state.well.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Well Organism Samples...</h4>
                )} 
              </div> 
              {(this.state.dnaSamples.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of organism sample nodes inside {this.state.well.name}:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    <Link to="/organism-samples/new" className="list-group-item list-group-item-action bg-success text-light">Add New Organism Sample</Link>
                    {organismSamples}
                  </ul>
                </div>  
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    There are currently no organism samples inside {this.state.well.name || 'this Well'}.
                    &nbsp;<Link to={`/organism-samples/new`}>Add New Organism Sample</Link>.
                  </p>
                </div>
              )}
            </div>  

            <div className="card mt-3">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.well).length > 0) ? (
                  <h4 className="card-title mb-0">Samples In {this.state.well.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Well Samples...</h4>
                )} 
              </div> 
              {(this.state.dnaSamples.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of sample nodes inside {this.state.well.name}:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    <Link to="/samples/new" className="list-group-item list-group-item-action bg-success text-light">Add New Sample</Link>
                    {samples}
                  </ul>
                </div>  
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    There are currently no samples inside {this.state.well.name || 'this Well'}.
                    &nbsp;<Link to={`/samples/new`}>Add New Sample</Link>.
                  </p>
                </div>
              )}
            </div>              
            
          </div>
        </div>

      </div>
    );
  }
}

export default WellProfile;