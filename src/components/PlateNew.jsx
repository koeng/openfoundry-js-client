import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';
import shortid from 'shortid';

class PlateNew extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      redirectTo: "",
      freezers: [],
      freezer: {},
      newPlate: {
        parentId: "",
        name: "",
        description: "",
        storageTemperature: -80,
        plateType: "",
        potentialLocation: [],
        parentLocation: ""
      }
    };
    this.getAllFreezers = this.getAllFreezers.bind(this);
    this.updateFreezer = this.updateFreezer.bind(this);
    this.updatePlate = this.updatePlate.bind(this);
    this.submitNewPlate = this.submitNewPlate.bind(this);
    this.handleNewPlateFormSubmit = this.handleNewPlateFormSubmit.bind(this);
  }

  getAllFreezers() {
    axios.get(`https://api.biohacking.services/freezers`)
    .then(res => {
      //console.log(res.data);
      this.setState({
        freezers: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  updateFreezer(e) {
    let newPlate = this.state.newPlate;
    let freezers = this.state.freezers;
    let freezer;
    for(let i = 0; i < freezers.length; i++){
      let thisFreezer = freezers[i];
      if (thisFreezer._id === e.target.value){
        freezer = thisFreezer;
        newPlate.parentId = freezer._id;
      }
    }
    this.setState({
      newPlate,
      freezer
    });
  }

  updatePlate(e) {
    const field = e.target.name;
    const newPlate = this.state.newPlate;
    if(field === 'storageTemperature') {
      newPlate[field] = Number(e.target.value);
    } else {
      newPlate[field] = e.target.value;
    }
    this.setState({
      newPlate
    });    
  }

  submitNewPlate(plate) {
    if(plate.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post('https://api.biohacking.services/plates/new', plate, config)
      .then(res => {
        //console.log(res.data)        
        this.setState({ redirect: true });
      })
      .catch(error => {
        console.error(error);
        this.setState({ newPlate: plate });
      });      
    }  
  }

  handleNewPlateFormSubmit(e) {
    e.preventDefault();
    let newPlate = this.state.newPlate;
    let parentId = newPlate.parentId;
    this.setState({
      redirectTo: `/freezers/${parentId}`,
      newPlate: {
        parentId: "",
        name: "",
        description: "",
        storageTemperature: -80,
        plateType: ""   
      }
    });
    this.submitNewPlate(newPlate);
  }

  componentDidMount() {
    this.getAllFreezers();
  }

  render() { 
    const freezers = this.state.freezers.map((freezer, index) => {
      return (
        <option 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          value={freezer._id}
        >
          {freezer.name}
        </option>
      )
    });   
    if (this.state.redirect === true) {
      return ( <Redirect to={this.state.redirectTo}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col col-md-7">

            { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
              <div className="card mt-3">
                <div className="card-header bg-dark text-light">
                  <h4 className="card-title mb-0">New Plate</h4>
                </div>
                <div className="card-body">
                  <form onSubmit={this.handleNewPlateFormSubmit}>

                    <div className="form-group">
                      <label htmlFor="parentId">Freezer</label>
                      <select 
                        name="parentId"
                        className="form-control"
                        value={this.state.newPlate.parentId || null}
                        onChange={this.updateFreezer}
                      >
                        <option>Select One</option>
                        {freezers}
                      </select>
                    </div>

                    <div className="form-group">
                      <label htmlFor="name">Name</label>
                      <input 
                        name="name"
                        className="form-control"
                        value={this.state.newPlate.name}
                        onChange={this.updatePlate}
                        placeholder="Plate Name"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="name">Description</label>
                      <input 
                        name="description"
                        type="text"
                        className="form-control"
                        value={this.state.newPlate.description}
                        onChange={this.updatePlate}
                        placeholder="A short description of the Plate."
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="name">Storage Temperature (C)</label>
                      <input 
                        name="storageTemperature"
                        type="number"
                        className="form-control"
                        value={this.state.newPlate.storageTemperature}
                        onChange={this.updatePlate}
                        min="-80"
                        step="1"
                      />
                    </div>

                    <div className="form-group text-center">
                      <div className="btn-group" role="group" aria-label="Basic example">
                        <Link to="/plates" className="btn btn-secondary mt-3">Back</Link>
                        <button type="submit" className="btn btn-success mt-3">Submit</button>
                      </div>  
                    </div>                    

                  </form>
                </div>
              </div>
            ) : null }  
          </div>
        </div>
      </div>
    );
  }
}

export default PlateNew;