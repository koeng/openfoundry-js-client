import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class WorldEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      worldId: "",
      world: {
        name: "",
        description: ""
      }
    };
    this.getWorld = this.getWorld.bind(this);
    this.updateWorld = this.updateWorld.bind(this);
    this.submitEditWorld = this.submitEditWorld.bind(this);
    this.handleNewWorldFormSubmit = this.handleNewWorldFormSubmit.bind(this);
  }

  getWorld() {
    const worldId = this.props.match.params.worldId;
    axios.get(`https://api.biohacking.services/worlds/${worldId}`)
    .then(res => {
      //console.log(res.data);     
      this.setState({
        world: res.data.world
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  updateWorld(e) {
    const field = e.target.name;
    let world = this.state.world;
    world[field] = e.target.value;
    this.setState({
      world
    });    
  }

  submitEditWorld(world) {
    const worldId = this.props.match.params.worldId;
    if(world.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post(`https://api.biohacking.services/worlds/${worldId}/edit`, world, config)
      .then(res => {       
        this.setState({
          redirect: true
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ world });
      });      
    }  
  }

  handleNewWorldFormSubmit(e) {
    e.preventDefault();
    let world = this.state.world;
    this.submitEditWorld(world);
  }

  componentDidMount() {
    this.getWorld();
  }

  render() {
    const worldId = this.props.match.params.worldId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/worlds/${worldId}`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.world).length > 0) ? (
                  <h4 className="card-title mb-0">Edit {this.state.world.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading World...</h4>
                )}  
              </div>
              {(Object.keys(this.state.world).length > 0) ? (
                <div>
                  <div className="card-body">
                    <form onSubmit={this.handleNewWorldFormSubmit}>
                      <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input 
                          name="name"
                          className="form-control"
                          value={this.state.world.name}
                          onChange={this.updateWorld}
                          placeholder="World Name"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <input 
                          name="description"
                          className="form-control"
                          value={this.state.world.description}
                          onChange={this.updateWorld}
                          placeholder="A short description of the World."
                        />
                      </div>


                      <div className="form-group text-center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                          <Link to={`/worlds/${worldId}`} className="btn btn-secondary mt-3">Back</Link>
                          <Link to={`/worlds/${worldId}/delete`} className="btn btn-danger mt-3">Delete</Link>
                          <button type="submit" className="btn btn-success mt-3">Save Changes</button>
                        </div>
                      </div>                    
                                         
                    </form>    

                  </div>
                </div>
              ) : null }  
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default WorldEdit;