import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class TransformConsolidate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      attribute: 'Sequence Verified',
      allPlates: true,
      plates: []
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onUpdateAttribute = this.onUpdateAttribute.bind(this);
    this.onUpdateAllPlates = this.onUpdateAllPlates.bind(this);
    this.onPlateToggle = this.onPlateToggle.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    alert('ToDo: Handle Submit Data To Executor');
  }

  onUpdateAttribute(e) {
    this.setState({
      attribute: e.target.value
    });
  }

  onUpdateAllPlates(e) {
    this.setState({
      allPlates: e.target.value === 'true'
    });
  }

  onPlateToggle(e) {
    let plates = this.state.plates;
    let plateIndex = plates.indexOf(e.target.value);
    if (plateIndex > -1) {
      plates.splice(plateIndex, 1);
    } else {
      plates.push(e.target.value);
    }
    this.setState({
      plates
    });
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Consolidate Plates</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
           
                    <form onSubmit={this.onFormSubmit}>
                      
                      <h4 className="card-title">
                        Choose Attribute To Consolidate On:
                      </h4>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="radio" 
                          name="attribute" 
                          id="attribute-sequence-verified" 
                          value="Sequence Verified" 
                          checked={this.state.attribute === 'Sequence Verified'}
                          onChange={this.onUpdateAttribute}
                        />
                        <label className="form-check-label" htmlFor="attribute-sequence-verified" style={{'width': '150px'}}>
                          Sequence Verified
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="radio" 
                          name="attribute" 
                          id="attribute-collection" 
                          value="Collection" 
                          checked={this.state.attribute === 'Collection'}
                          onChange={this.onUpdateAttribute}
                        />
                        <label className="form-check-label" htmlFor="attribute-collection" style={{'width': '150px'}}>
                          Collection
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="radio" 
                          name="attribute" 
                          id="attribute-source-organism" 
                          value="Source Organism" 
                          checked={this.state.attribute === 'Source Organism'}
                          onChange={this.onUpdateAttribute}
                        />
                        <label className="form-check-label" htmlFor="attribute-source-organism" style={{'width': '150px'}}>
                          Source Organism
                        </label>
                      </div>

                      <h4 className="card-title mt-3">
                        Select Plates:
                      </h4>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="radio" 
                          name="allPlates" 
                          id="all-plates-true" 
                          value="true" 
                          checked={this.state.allPlates === true}
                          onChange={this.onUpdateAllPlates}
                        />
                        <label className="form-check-label" htmlFor="all-plates-true" style={{'width': '150px'}}>
                          All Plates
                        </label>
                      </div> 

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="radio" 
                          name="allPlates" 
                          id="all-plates-false" 
                          value="false" 
                          checked={this.state.allPlates === false}
                          onChange={this.onUpdateAllPlates}
                        />
                        <label className="form-check-label" htmlFor="all-plates-false" style={{'width': '150px'}}>
                          Select Plates
                        </label>
                      </div>                      

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate1" 
                          id="plate1"
                          onChange={this.onPlateToggle}
                          disabled={this.state.allPlates === true}
                        />
                        <label className="form-check-label" htmlFor="plate1">
                          Plate 1
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate2" 
                          id="plate2"
                          onChange={this.onPlateToggle}
                          disabled={this.state.allPlates === true}
                        />
                        <label className="form-check-label" htmlFor="plate2">
                          Plate 2
                        </label>
                      </div>

                      <div className="form-group mt-3">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <Link to="/transform" className="btn btn-block btn-light">Transform Menu</Link>
                          <button type="submit" className="btn btn-block btn-success">Confirm</button>
                        </div>  
                      </div>
                    </form> 
                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default TransformConsolidate;
