import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class WorldNew extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      newWorld: {
        name: "",
        description: ""
      }
    };
    this.updateWorld = this.updateWorld.bind(this);
    this.submitNewWorld = this.submitNewWorld.bind(this);
    this.handleNewWorldFormSubmit = this.handleNewWorldFormSubmit.bind(this);
  }

  updateWorld(e) {
    const field = e.target.name;
    const newWorld = this.state.newWorld;
    newWorld[field] = e.target.value;
    this.setState({
      newWorld
    });    
  }

  submitNewWorld(world) {
    if(world.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post('https://api.biohacking.services/worlds/new', world, config)
      .then(res => {
        //console.log(res.data)        
        this.setState({ redirect: true });
      })
      .catch(error => {
        console.error(error);
        this.setState({ newWorld: world });
      });      
    }  
  }

  handleNewWorldFormSubmit(e) {
    e.preventDefault();
    let newWorld = this.state.newWorld;
    this.setState({
      newWorld: {
        name: "",
        description: ""      
      }
    });
    this.submitNewWorld(newWorld);
  }

  render() { 
    if (this.state.redirect === true) {
      return ( <Redirect to="/worlds"/> )
    }
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col col-md-7">

            { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
              <div className="card mt-3">
                <div className="card-header bg-dark text-light">
                  <h4 className="card-title mb-0">New World</h4>
                </div>
                <div className="card-body">
                  <form onSubmit={this.handleNewWorldFormSubmit}>
                    <div className="form-group">
                      <label htmlFor="name">Name</label>
                      <input 
                        name="name"
                        className="form-control"
                        value={this.state.newWorld.name}
                        onChange={this.updateWorld}
                        placeholder="World Name"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="name">Description</label>
                      <input 
                        name="description"
                        className="form-control"
                        value={this.state.newWorld.description}
                        onChange={this.updateWorld}
                        placeholder="A short description of the World."
                      />
                    </div>

                    <div className="form-group text-center">
                      <div className="btn-group" role="group" aria-label="Basic example">
                        <Link to="/worlds" className="btn btn-secondary mt-3">Back</Link>
                        <button type="submit" className="btn btn-success mt-3">Submit</button>
                      </div>  
                    </div>                    

                  </form>
                </div>
              </div>
            ) : null }  
          </div>
        </div>
      </div>
    );
  }
}

export default WorldNew;