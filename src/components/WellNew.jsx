import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';
import shortid from 'shortid';

class WellNew extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      redirectTo: "",
      plates: [],
      plate: {},
      newWell: {
        parentId: "",
        name: "",
        description: "",
        volume: 0
      }
    };
    this.getAllPlates = this.getAllPlates.bind(this);
    this.updatePlate = this.updatePlate.bind(this);
    this.updateWell = this.updateWell.bind(this);
    this.submitNewWell = this.submitNewWell.bind(this);
    this.handleNewWellFormSubmit = this.handleNewWellFormSubmit.bind(this);
  }

  getAllPlates() {
    axios.get(`https://api.biohacking.services/plates`)
    .then(res => {
      //console.log(res.data);
      this.setState({
        plates: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  updatePlate(e) {
    let newWell = this.state.newWell;
    let plates = this.state.plates;
    let plate;
    for(let i = 0; i < plates.length; i++){
      let thisPlate = plates[i];
      if (thisPlate._id === e.target.value){
        plate = thisPlate;
        newWell.parentId = plate._id;
      }
    }
    this.setState({
      newWell,
      plate
    });
  }

  updateWell(e) {
    const field = e.target.name;
    const newWell = this.state.newWell;
    if(field === 'volume') {
      newWell[field] = Number(e.target.value);
    } else {
      newWell[field] = e.target.value;
    }
    this.setState({
      newWell
    });    
  }

  submitNewWell(well) {
    if(well.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post('https://api.biohacking.services/wells/new', well, config)
      .then(res => {
        //console.log(res.data)        
        this.setState({ redirect: true });
      })
      .catch(error => {
        console.error(error);
        this.setState({ newWell: well });
      });      
    }  
  }

  handleNewWellFormSubmit(e) {
    e.preventDefault();
    let newWell = this.state.newWell;
    let parentId = newWell.parentId;
    this.setState({
      redirectTo: `/plates/${parentId}`,
      newWell: {
        parentId: "",
        name: "",
        description: "",
        volume: 0   
      }
    });
    this.submitNewWell(newWell);
  }

  componentDidMount() {
    this.getAllPlates();
  }

  render() { 
    const plates = this.state.plates.map((plate, index) => {
      return (
        <option 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          value={plate._id}
        >
          {plate.name}
        </option>
      )
    });   
    if (this.state.redirect === true) {
      return ( <Redirect to={this.state.redirectTo}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col col-md-7">

            { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
              <div className="card mt-3">
                <div className="card-header bg-dark text-light">
                  <h4 className="card-title mb-0">New Well</h4>
                </div>
                <div className="card-body">
                  <form onSubmit={this.handleNewWellFormSubmit}>

                    <div className="form-group">
                      <label htmlFor="parentId">Plate</label>
                      <select 
                        name="parentId"
                        className="form-control"
                        value={this.state.newWell.parentId || null}
                        onChange={this.updatePlate}
                      >
                        <option>Select One</option>
                        {plates}
                      </select>
                    </div>

                    <div className="form-group">
                      <label htmlFor="name">Name</label>
                      <input 
                        name="name"
                        className="form-control"
                        value={this.state.newWell.name}
                        onChange={this.updateWell}
                        placeholder="Well Name"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="description">Description</label>
                      <input 
                        name="description"
                        type="text"
                        className="form-control"
                        value={this.state.newWell.description}
                        onChange={this.updateWell}
                        placeholder="A short description of the Well."
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="volume">Volume</label>
                      <input 
                        name="volume"
                        type="number"
                        className="form-control"
                        value={this.state.newWell.volume}
                        onChange={this.updateWell}
                        min="0"
                        step="1"
                      />
                    </div>

                    <div className="form-group text-center">
                      <div className="btn-group" role="group" aria-label="Basic example">
                        <Link to="/wells" className="btn btn-secondary mt-3">Back</Link>
                        <button type="submit" className="btn btn-success mt-3">Submit</button>
                      </div>  
                    </div>                    

                  </form>
                </div>
              </div>
            ) : null }  
          </div>
        </div>
      </div>
    );
  }
}

export default WellNew;