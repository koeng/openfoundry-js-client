import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class PlateList extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      plates: []
    };
    this.getAllPlates = this.getAllPlates.bind(this);
  }

  getAllPlates() {
    axios.get(`https://api.biohacking.services/plates`)
    .then(res => {
      this.setState({
        plates: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  componentDidMount() {
    this.getAllPlates();
  }

  render() {
    const plates = this.state.plates.map((plate, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/plates/${plate._id}`}
        >
          {plate.name}
        </Link>
      )
    });    
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">
            
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Plates</h4>
              </div>
              {(this.state.plates.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of plate nodes:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-success text-light"
                        to="/plates/new"
                      >Add New Plate</Link>  
                    ) : null }
                    { plates }
                  </ul>
                </div>  
              ) : (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      There are currently no plate nodes.
                    </p>
                  </div>
                  <ul className="list-group list-group-flush">
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-success text-light"
                        to="/plates/new"
                      >Add New Plate</Link>  
                    ) : null }
                  </ul>                  
                </div>  
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PlateList;