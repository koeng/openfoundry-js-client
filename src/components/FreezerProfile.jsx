import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class FreezerProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      freezerId: "",
      world: {},
      freezer: {},
      plates: []
    };
    this.getFreezer = this.getFreezer.bind(this);
  }

  getFreezer() {
    const freezerId = this.props.match.params.freezerId;
    axios.get(`https://api.biohacking.services/freezers/${freezerId}`)
    .then(res => {
      //console.log(res.data);     
      this.setState({
        freezer: res.data.data,
        world: res.data.data.parentId,
        plates: res.data.children
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  componentDidMount() {
    this.getFreezer();
  }

  render() {
    const plates = this.state.plates.map((plate, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/plates/${plate._id}`}
        >
          {plate.name}
        </Link>
      )
    }); 
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">

            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.freezer).length > 0) ? (
                  <h4 className="card-title mb-0">{this.state.freezer.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Freezer...</h4>
                )}  
              </div>
              {(Object.keys(this.state.freezer).length > 0) ? (
                <div>
                  <div className="card-body">

                    <p className="card-text">
                      { this.state.freezer.description || "No description provided." }
                    </p>

                  </div>
                  <ul className="list-group list-group-flush">
                  <Link 
                      className="list-group-item list-group-item-action bg-light"
                      to={`/worlds/${this.state.world._id}`}
                    >World - {this.state.world.name}</Link> 
                    <Link 
                      className="list-group-item list-group-item-action bg-secondary text-light"
                      to="/freezers"
                    >Freezer List</Link>                    
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-primary text-light"
                        to={`/freezers/${this.state.freezer._id}/edit`}
                      >Edit Freezer</Link>
                    ) : null }
                  </ul>
                </div>
              ) : null }  
            </div>

            <div className="card mt-3">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.freezer).length > 0) ? (
                  <h4 className="card-title mb-0">Plates In {this.state.freezer.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Freezer Plates...</h4>
                )} 
              </div> 
              {(this.state.plates.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of plate nodes inside {this.state.freezer.name}:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    <Link to="/plates/new" className="list-group-item list-group-item-action bg-success text-light">Add New Plate</Link>
                    {plates}
                  </ul>
                </div>  
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    There are currently no Plates inside {this.state.freezer.name || 'this Freezer'}.
                    &nbsp;<Link to={`/plates/new`}>Add New Plate</Link>.
                  </p>
                </div>
              )}
            </div>   
          </div>
        </div>

      </div>
    );
  }
}

export default FreezerProfile;