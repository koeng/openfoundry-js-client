import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class TransformPick extends Component {

  constructor(props) {
    super(props);
    this.state = {
      colonyCount: 0,
      plates: []
    };
    this.updateColonyCount = this.updateColonyCount.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onPlateToggle = this.onPlateToggle.bind(this);
  }

  updateColonyCount(e) {
    this.setState({
      colonyCount: Number(e.target.value)
    });
  }

  onFormSubmit(e) {
    e.preventDefault();
    alert('ToDo: Handle Submit Data To Executor');
  }

  onPlateToggle(e) {
    let plates = this.state.plates;
    let plateIndex = plates.indexOf(e.target.value);
    if (plateIndex > -1) {
      plates.splice(plateIndex, 1);
    } else {
      plates.push(e.target.value);
    }
    this.setState({
      plates
    });
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Pick</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
           
                    <form onSubmit={this.onFormSubmit}>
                      
                      <h4 className="card-title">
                        Select Number Of Colonies To Pick:
                      </h4>

                      <div className="form-group">
                      <label className="form-check-label" htmlFor="colonyCount">
                          Number Of Colonies
                        </label>
                        <input 
                          className="form-control" 
                          type="number" 
                          value={this.state.colonyCount}
                          id="colonyCount"
                          onChange={this.updateColonyCount}
                          min="0"
                          max="100"
                          step="1"
                        />
                        <small id="colonyCountHelp" className="form-text text-muted">
                          Set Count To 0 To Select As Many As Possible
                        </small>
                      </div>

                      <h4 className="card-title">
                        Select Plates To Pick:
                      </h4>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="transform1" 
                          id="transform1"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="transform1">
                          Transform 1
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="transform2" 
                          id="transform2"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="transform2">
                          Transform 2
                        </label>
                      </div>

                      <div className="form-group mt-3">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <Link to="/transform" className="btn btn-block btn-light">Transform Menu</Link>
                          <button type="submit" className="btn btn-block btn-success">Confirm</button>
                        </div>  
                      </div>
                    </form> 
                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default TransformPick;
