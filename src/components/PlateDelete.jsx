import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class PlateDelete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      plate: {}
    };
    this.getPlate = this.getPlate.bind(this);
    this.removePlate = this.removePlate.bind(this);
  }

  getPlate() {
    const plateId = this.props.match.params.plateId;
    axios.get(`https://api.biohacking.services/plates/${plateId}`)
    .then(res => {     
      this.setState({
        plate: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  removePlate() {
    const plateId = this.props.match.params.plateId;

    let config = {
      'headers': {
        'authorization': `Bearer ${Auth.getToken()}`,
        //'Content-Type': 'application/x-www-form-urlencoded'
      },
      'json': true
    };  
    axios.post(`https://api.biohacking.services/plates/${plateId}/remove`, plateId, config)
    .then(res => {       
      this.setState({
        redirect: true
      });
    })
    .catch(error => {
      console.error(error);
    });      
      
  }

  componentDidMount() {
    this.getPlate();
  }

  render() {
    const plateId = this.props.match.params.plateId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/plates`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.plate).length > 0) ? (
                  <h4 className="card-title mb-0">Delete {this.state.plate.name}?</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Plate...</h4>
                )}  
              </div>
              {(Object.keys(this.state.plate).length > 0) ? (       
                <div className="card-body">
                  <p className="card-text">
                    Are you sure you want to Delete {this.state.plate.name}? This action cannot be undone.
                  </p>
                </div>
              ) : null }  
              <div className="form-group text-center">
                <div className="btn-group" role="group">
                  <Link to={`/plates/${plateId}/edit`} className="btn btn-secondary mt-3">Back</Link>
                  <button 
                    className="btn btn-danger mt-3"
                    onClick={this.removePlate}
                  >
                    Delete {this.state.plate.name}
                  </button>
                </div>
              </div>               
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default PlateDelete;