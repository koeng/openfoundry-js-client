import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class FreezerEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      freezerId: "",
      freezer: {
        parentId: "",
        name: "",
        description: "",
        temperature: 0,
        potentialLocation: [],
        parentLocation: ""
      }
    };
    this.getFreezer = this.getFreezer.bind(this);
    this.updateFreezer = this.updateFreezer.bind(this);
    this.submitEditFreezer = this.submitEditFreezer.bind(this);
    this.handleNewFreezerFormSubmit = this.handleNewFreezerFormSubmit.bind(this);
  }

  getFreezer() {
    const freezerId = this.props.match.params.freezerId;
    axios.get(`https://api.biohacking.services/freezers/${freezerId}`)
    .then(res => {     
      this.setState({
        freezer: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }


  updateFreezer(e) {
    const field = e.target.name;
    let freezer = this.state.freezer;
    freezer[field] = e.target.value;
    this.setState({
      freezer
    });    
  }

  submitEditFreezer(freezer) {
    const freezerId = this.props.match.params.freezerId;
    if(freezer.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post(`https://api.biohacking.services/freezers/${freezerId}/edit`, freezer, config)
      .then(res => {       
        this.setState({
          redirect: true
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ freezer });
      });      
    }  
  }

  handleNewFreezerFormSubmit(e) {
    e.preventDefault();
    let freezer = this.state.freezer;
    this.submitEditFreezer(freezer);
  }

  componentDidMount() {
    this.getFreezer();
  }

  render() {
    const freezerId = this.props.match.params.freezerId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/freezers/${freezerId}`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.freezer).length > 0) ? (
                  <h4 className="card-title mb-0">Edit {this.state.freezer.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Freezer...</h4>
                )}  
              </div>
              {(Object.keys(this.state.freezer).length > 0) ? (
                <div>
                  <div className="card-body">
                    <form onSubmit={this.handleNewFreezerFormSubmit}>
                      <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input 
                          name="name"
                          className="form-control"
                          value={this.state.freezer.name}
                          onChange={this.updateFreezer}
                          placeholder="Freezer Name"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <input 
                          name="description"
                          className="form-control"
                          value={this.state.freezer.description}
                          onChange={this.updateFreezer}
                          placeholder="A short description of the Freezer."
                        />
                      </div>


                      <div className="form-group text-center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                          <Link to={`/freezers/${freezerId}`} className="btn btn-secondary mt-3">Back</Link>
                          <Link to={`/freezers/${freezerId}/delete`} className="btn btn-danger mt-3">Delete</Link>
                          <button type="submit" className="btn btn-success mt-3">Save Changes</button>
                        </div>
                      </div>                    
                                         
                    </form>    

                  </div>
                </div>
              ) : null }  
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default FreezerEdit;