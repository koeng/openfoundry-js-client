import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class DnaSampleDelete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      sample: {}
    };
    this.getSample = this.getSample.bind(this);
    this.removeSample = this.removeSample.bind(this);
  }

  getSample() {
    const sampleId = this.props.match.params.sampleId;
    axios.get(`https://api.biohacking.services/dna-samples/${sampleId}`)
    .then(res => {     
      this.setState({
        sample: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  removeSample() {
    const sampleId = this.props.match.params.sampleId;

    let config = {
      'headers': {
        'authorization': `Bearer ${Auth.getToken()}`,
        //'Content-Type': 'application/x-www-form-urlencoded'
      },
      'json': true
    };  
    axios.post(`https://api.biohacking.services/dna-samples/${sampleId}/remove`, sampleId, config)
    .then(res => {       
      this.setState({
        redirect: true
      });
    })
    .catch(error => {
      console.error(error);
    });      
      
  }

  componentDidMount() {
    this.getSample();
  }

  render() {
    const sampleId = this.props.match.params.sampleId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/dna-samples`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.sample).length > 0) ? (
                  <h4 className="card-title mb-0">Delete {this.state.sample.name}?</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Sample...</h4>
                )}  
              </div>
              {(Object.keys(this.state.sample).length > 0) ? (       
                <div className="card-body">
                  <p className="card-text">
                    Are you sure you want to Delete {this.state.sample.name}? This action cannot be undone.
                  </p>
                </div>
              ) : null }  
              <div className="form-group text-center">
                <div className="btn-group" role="group">
                  <Link to={`/dna-samples/${sampleId}/edit`} className="btn btn-secondary mt-3">Back</Link>
                  <button 
                    className="btn btn-danger mt-3"
                    onClick={this.removeSample}
                  >
                    Delete {this.state.sample.name}
                  </button>
                </div>
              </div>               
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default DnaSampleDelete;