import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class WorldDelete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      world: {}
    };
    this.getWorld = this.getWorld.bind(this);
    this.removeWorld = this.removeWorld.bind(this);
  }

  getWorld() {
    const worldId = this.props.match.params.worldId;
    axios.get(`https://api.biohacking.services/worlds/${worldId}`)
    .then(res => {
      //console.log(res.data);     
      this.setState({
        world: res.data.world
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  removeWorld() {
    const worldId = this.props.match.params.worldId;

    let config = {
      'headers': {
        'authorization': `Bearer ${Auth.getToken()}`,
        //'Content-Type': 'application/x-www-form-urlencoded'
      },
      'json': true
    };  
    axios.post(`https://api.biohacking.services/worlds/${worldId}/remove`, worldId, config)
    .then(res => {       
      this.setState({
        redirect: true
      });
    })
    .catch(error => {
      console.error(error);
    });      
      
  }

  componentDidMount() {
    this.getWorld();
  }

  render() {
    const worldId = this.props.match.params.worldId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/worlds`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.world).length > 0) ? (
                  <h4 className="card-title mb-0">Delete {this.state.world.name}?</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading World...</h4>
                )}  
              </div>
              {(Object.keys(this.state.world).length > 0) ? (       
                <div className="card-body">
                  <p className="card-text">
                    Are you sure you want to Delete {this.state.world.name}? This action cannot be undone.
                  </p>
                </div>
              ) : null }  
              <div className="form-group text-center">
                <div className="btn-group" role="group" aria-label="Basic example">
                  <Link to={`/worlds/${worldId}/edit`} className="btn btn-secondary mt-3">Back</Link>
                  <button 
                    className="btn btn-danger mt-3"
                    onClick={this.removeWorld}
                  >
                    Delete {this.state.world.name}
                  </button>
                </div>
              </div>               
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default WorldDelete;