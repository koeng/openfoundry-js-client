import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class FreezerDelete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      freezer: {}
    };
    this.getFreezer = this.getFreezer.bind(this);
    this.removeFreezer = this.removeFreezer.bind(this);
  }

  getFreezer() {
    const freezerId = this.props.match.params.freezerId;
    axios.get(`https://api.biohacking.services/freezers/${freezerId}`)
    .then(res => {     
      this.setState({
        freezer: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  removeFreezer() {
    const freezerId = this.props.match.params.freezerId;

    let config = {
      'headers': {
        'authorization': `Bearer ${Auth.getToken()}`,
        //'Content-Type': 'application/x-www-form-urlencoded'
      },
      'json': true
    };  
    axios.post(`https://api.biohacking.services/freezers/${freezerId}/remove`, freezerId, config)
    .then(res => {       
      this.setState({
        redirect: true
      });
    })
    .catch(error => {
      console.error(error);
    });      
      
  }

  componentDidMount() {
    this.getFreezer();
  }

  render() {
    const freezerId = this.props.match.params.freezerId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/freezers`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.freezer).length > 0) ? (
                  <h4 className="card-title mb-0">Delete {this.state.freezer.name}?</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Freezer...</h4>
                )}  
              </div>
              {(Object.keys(this.state.freezer).length > 0) ? (       
                <div className="card-body">
                  <p className="card-text">
                    Are you sure you want to Delete {this.state.freezer.name}? This action cannot be undone.
                  </p>
                </div>
              ) : null }  
              <div className="form-group text-center">
                <div className="btn-group" role="group">
                  <Link to={`/freezers/${freezerId}/edit`} className="btn btn-secondary mt-3">Back</Link>
                  <button 
                    className="btn btn-danger mt-3"
                    onClick={this.removeFreezer}
                  >
                    Delete {this.state.freezer.name}
                  </button>
                </div>
              </div>               
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default FreezerDelete;