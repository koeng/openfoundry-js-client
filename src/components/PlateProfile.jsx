import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class PlateProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      plateId: "",
      freezer: {},
      plate: {},
      wells: []
    };
    this.getPlate = this.getPlate.bind(this);
  }

  getPlate() {
    const plateId = this.props.match.params.plateId;
    axios.get(`https://api.biohacking.services/plates/${plateId}`)
    .then(res => {
      //console.log(res.data);     
      this.setState({
        plate: res.data.data,
        freezer: res.data.data.parentId,
        wells: res.data.children
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  componentDidMount() {
    this.getPlate();
  }

  render() {
    const wells = this.state.wells.map((well, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/wells/${well._id}`}
        >
          {well.name}
        </Link>
      )
    }); 
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">

            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.plate).length > 0) ? (
                  <h4 className="card-title mb-0">{this.state.plate.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Plate...</h4>
                )}  
              </div>
              {(Object.keys(this.state.freezer).length > 0) ? (
                <div>
                  <div className="card-body">

                    <p className="card-text">
                      { this.state.plate.description || "No description provided." }<br/>
                      Storage Temperature: {this.state.plate.storageTemperature}
                    </p>

                  </div>
                  <ul className="list-group list-group-flush">
                  <Link 
                      className="list-group-item list-group-item-action bg-light"
                      to={`/freezers/${this.state.freezer._id}`}
                    >Freezer - {this.state.freezer.name}</Link> 
                    <Link 
                      className="list-group-item list-group-item-action bg-secondary text-light"
                      to="/plates"
                    >Plate List</Link>                    
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-primary text-light"
                        to={`/plates/${this.state.plate._id}/edit`}
                      >Edit Plate</Link>
                    ) : null }
                  </ul>
                </div>
              ) : null }  
            </div>

            <div className="card mt-3">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.plate).length > 0) ? (
                  <h4 className="card-title mb-0">Wells In {this.state.plate.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Plate Wells...</h4>
                )} 
              </div> 
              {(this.state.wells.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of well nodes inside {this.state.plate.name}:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    <Link to="/wells/new" className="list-group-item list-group-item-action bg-success text-light">Add New Well</Link>
                    {wells}
                  </ul>
                </div>  
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    There are currently no Wells inside {this.state.plate.name || 'this Plate'}.
                    &nbsp;<Link to={`/wells/new`}>Add New Well</Link>.
                  </p>
                </div>
              )}
            </div>   
          </div>
        </div>

      </div>
    );
  }
}

export default PlateProfile;