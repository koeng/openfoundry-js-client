import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class WellEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      wellId: "",
      well: {
        parentId: "",
        name: "",
        description: "",
        volume: 0
      }
    };
    this.getWell = this.getWell.bind(this);
    this.updateWell = this.updateWell.bind(this);
    this.submitEditWell = this.submitEditWell.bind(this);
    this.handleNewWellFormSubmit = this.handleNewWellFormSubmit.bind(this);
  }

  getWell() {
    const wellId = this.props.match.params.wellId;
    axios.get(`https://api.biohacking.services/wells/${wellId}`)
    .then(res => {     
      this.setState({
        well: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }


  updateWell(e) {
    const field = e.target.name;
    let well = this.state.well;
    if(field === 'volume') {
      well[field] = Number(e.target.value);
    } else {
      well[field] = e.target.value;
    }
    this.setState({
      well
    });    
  }

  submitEditWell(well) {
    const wellId = this.props.match.params.wellId;
    if(well.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post(`https://api.biohacking.services/wells/${wellId}/edit`, well, config)
      .then(res => {       
        this.setState({
          redirect: true
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ well });
      });      
    }  
  }

  handleNewWellFormSubmit(e) {
    e.preventDefault();
    let well = this.state.well;
    this.submitEditWell(well);
  }

  componentDidMount() {
    this.getWell();
  }

  render() {
    const wellId = this.props.match.params.wellId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/wells/${wellId}`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.well).length > 0) ? (
                  <h4 className="card-title mb-0">Edit {this.state.well.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Well...</h4>
                )}  
              </div>
              {(Object.keys(this.state.well).length > 0) ? (
                <div>
                  <div className="card-body">
                    <form onSubmit={this.handleNewWellFormSubmit}>
                      <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input 
                          name="name"
                          className="form-control"
                          value={this.state.well.name}
                          onChange={this.updateWell}
                          placeholder="Well Name"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <input 
                          name="description"
                          className="form-control"
                          value={this.state.well.description}
                          onChange={this.updateWell}
                          placeholder="A short description of the Well."
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="volume">Volume</label>
                        <input 
                          name="volume"
                          type="number"
                          className="form-control"
                          value={this.state.well.volume}
                          onChange={this.updateWell}
                          min="0"
                          step="1"
                        />
                      </div>

                      <div className="form-group text-center">
                        <div className="btn-group" role="group">
                          <Link to={`/wells/${wellId}`} className="btn btn-secondary mt-3">Back</Link>
                          <Link to={`/wells/${wellId}/delete`} className="btn btn-danger mt-3">Delete</Link>
                          <button type="submit" className="btn btn-success mt-3">Save Changes</button>
                        </div>
                      </div>                    
                                         
                    </form>    

                  </div>
                </div>
              ) : null }  
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default WellEdit;