import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class DnaSampleList extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      samples: []
    };
    this.getAllDnaSamples = this.getAllDnaSamples.bind(this);
  }

  getAllDnaSamples() {
    axios.get(`https://api.biohacking.services/dna-samples`)
    .then(res => {
      //console.log(res.data);
      this.setState({
        samples: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  componentDidMount() {
    this.getAllDnaSamples();
  }

  render() {
    const samples = this.state.samples.map((sample, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/dna-samples/${sample._id}`}
        >
          {sample.name}
        </Link>
      )
    });    
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">
            
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">DNA Samples</h4>
              </div>
              {(this.state.samples.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of DNA sample nodes:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-success text-light"
                        to="/dna-samples/new"
                      >Add New DNA Sample</Link>  
                    ) : null }
                    { samples }
                  </ul>
                </div>  
              ) : (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      There are currently no DNA sample nodes.
                    </p>
                  </div>
                  <ul className="list-group list-group-flush">
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-success text-light"
                        to="/dna-samples/new"
                      >Add New DNA Sample</Link>  
                    ) : null }
                  </ul>                  
                </div>  
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DnaSampleList;