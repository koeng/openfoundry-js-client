import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';
import shortid from 'shortid';

class SampleNew extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      wells: [],
      well: {},
      newSample: {
        parentId: "",
        name: "",
        description: "",
        provenance: "",
        mta: ""
      }
    };
    this.getAllWells = this.getAllWells.bind(this);
    this.updateWell = this.updateWell.bind(this);
    this.updateSample = this.updateSample.bind(this);
    this.submitNewSample = this.submitNewSample.bind(this);
    this.handleNewSampleFormSubmit = this.handleNewSampleFormSubmit.bind(this);
  }

  getAllWells() {
    axios.get(`https://api.biohacking.services/wells`)
    .then(res => {
      //console.log(res.data);
      this.setState({
        wells: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  updateWell(e) {
    let newSample = this.state.newSample;
    let wells = this.state.wells;
    let well;
    for(let i = 0; i < wells.length; i++){
      let thisWell = wells[i];
      if (thisWell._id === e.target.value){
        well = thisWell;
        newSample.parentId = well._id;
      }
    }
    this.setState({
      newSample,
      well
    });
  }

  updateSample(e) {
    const field = e.target.name;
    const newSample = this.state.newSample;
    if(field === 'fooNumber') {
      newSample[field] = Number(e.target.value);
    } else {
      newSample[field] = e.target.value;
    }
    this.setState({
      newSample
    });    
  }

  submitNewSample(sample) {
    if(sample.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post('https://api.biohacking.services/samples/new', sample, config)
      .then(res => {
        console.log(res.data)        
        this.setState({ redirect: true });
      })
      .catch(error => {
        console.error(error);
        this.setState({ newSample: sample });
      });      
    }  
  }

  handleNewSampleFormSubmit(e) {
    e.preventDefault();
    let newSample = this.state.newSample;
    this.setState({
      newSample: {
        name: "",
        description: "",
        provenance: "",
        mta: ""      
      }
    });
    this.submitNewSample(newSample);
  }

  componentDidMount() {
    this.getAllWells();
  }

  render() { 
    const wells = this.state.wells.map((well, index) => {
      return (
        <option 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          value={well._id}
        >
          {well.name}
        </option>
      )
    });   
    if (this.state.redirect === true) {
      return ( <Redirect to="/samples"/> )
    }
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col col-md-7">

            { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
              <div className="card mt-3">
                <div className="card-header bg-dark text-light">
                  <h4 className="card-title mb-0">New Sample</h4>
                </div>
                <div className="card-body">
                  <form onSubmit={this.handleNewSampleFormSubmit}>

                    <div className="form-group">
                      <label htmlFor="parentId">Well</label>
                      <select 
                        name="parentId"
                        className="form-control"
                        value={this.state.newSample.parentId}
                        onChange={this.updateWell}
                      >
                        <option>Select One</option>
                        {wells}
                      </select>
                    </div>

                    <div className="form-group">
                      <label htmlFor="name">Name</label>
                      <input 
                        name="name"
                        className="form-control"
                        value={this.state.newSample.name}
                        onChange={this.updateSample}
                        placeholder="Sample Name"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="description">Description</label>
                      <input 
                        name="description"
                        type="text"
                        className="form-control"
                        value={this.state.newSample.description}
                        onChange={this.updateSample}
                        placeholder="A short description of the Sample."
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="provenance">Provenance</label>
                      <textarea 
                        name="provenance"
                        className="form-control"
                        value={this.state.newSample.provenance}
                        onChange={this.updateSample}
                        placeholder="Sample Provenance."
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="mta">MTA</label>
                      <select 
                        name="mta"
                        className="form-control"
                        value={this.state.newSample.mta}
                        onChange={this.updateSample}
                      >
                        <option>Select One</option>
                        <option value="OpenMTA">OpenMTA</option>
                        <option value="UBMTA">UBMTA</option>
                        <option value="Limbo">Limbo</option>
                      </select>
                    </div>

                    <div className="form-group text-center">
                      <div className="btn-group" role="group" aria-label="Basic example">
                        <Link to="/samples" className="btn btn-secondary mt-3">Back</Link>
                        <button type="submit" className="btn btn-success mt-3">Submit</button>
                      </div>  
                    </div>                    

                  </form>
                </div>
              </div>
            ) : null }  
          </div>
        </div>
      </div>
    );
  }
}

export default SampleNew;