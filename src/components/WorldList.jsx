import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class WorldList extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      worlds: []
    };
    this.getAllWorlds = this.getAllWorlds.bind(this);
  }

  getAllWorlds() {
    axios.get(`https://api.biohacking.services/worlds`)
    .then(res => {
      //console.log(res.data);
      this.setState({
        worlds: res.data.worlds
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  componentDidMount() {
    this.getAllWorlds();
  }

  render() {
    const worlds = this.state.worlds.map((world, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/worlds/${world._id}`}
        >
          {world.name}
        </Link>
      )
    });    
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">
            
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Worlds</h4>
              </div>
              <div className="card-body">
                <p className="card-text">
                  Here is the list of parent world nodes:
                </p>
              </div>
              <ul className="list-group list-group-flush">
                { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                  <Link 
                    className="list-group-item list-group-item-action bg-success text-light"
                    to="/worlds/new"
                  >Add New World</Link>  
                ) : null }
                { worlds }
              </ul>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default WorldList;