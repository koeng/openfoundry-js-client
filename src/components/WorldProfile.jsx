import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class WorldProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      worldId: "",
      world: {},
      freezers: []
    };
    this.getWorld = this.getWorld.bind(this);
  }

  getWorld() {
    const worldId = this.props.match.params.worldId;
    axios.get(`https://api.biohacking.services/worlds/${worldId}`)
    .then(res => {
      //console.log(res.data);     
      this.setState({
        world: res.data.world,
        freezers: res.data.freezers
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  componentDidMount() {
    this.getWorld();
  }

  render() {
    const freezers = this.state.freezers.map((freezer, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/freezers/${freezer._id}`}
        >
          {freezer.name}
        </Link>
      )
    });    
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.world).length > 0) ? (
                  <h4 className="card-title mb-0">{this.state.world.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading World...</h4>
                )}  
              </div>
              {(Object.keys(this.state.world).length > 0) ? (
                <div>
                  <div className="card-body">

                    <p className="card-text">
                      { this.state.world.description || "No description provided." }
                    </p>

                  </div>
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-secondary text-light"
                      to="/worlds"
                    >World List</Link>                    
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-primary text-light"
                        to={`/worlds/${this.state.world._id}/edit`}
                      >Edit World</Link>
                    ) : null }
                    { freezers }
                  </ul>
                </div>
              ) : null }  
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default WorldProfile;