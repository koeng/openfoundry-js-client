import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class OrganismSampleProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sampleId: "",
      well: {},
      sample: {}
    };
    this.getSample = this.getSample.bind(this);
  }

  getSample() {
    const sampleId = this.props.match.params.sampleId;
    axios.get(`https://api.biohacking.services/organism-samples/${sampleId}`)
    .then(res => {
      //console.log(res.data);     
      this.setState({
        sample: res.data.data,
        well: res.data.data.parentId,
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  componentDidMount() {
    this.getSample();
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">

            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.sample).length > 0) ? (
                  <h4 className="card-title mb-0">{this.state.sample.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Sample...</h4>
                )}  
              </div>
              {(Object.keys(this.state.sample).length > 0) ? (
                <div>
                  <div className="card-body">

                    <p className="card-text">
                      Description: { this.state.sample.description || "No description provided." }<br/>
                      Provenance: { this.state.sample.provenance || "No provenance provided" }<br/>
                      MTA: { this.state.sample.mta || "Limbo" }
                    </p>

                  </div>
                  <ul className="list-group list-group-flush">
                  <Link 
                      className="list-group-item list-group-item-action bg-light"
                      to={`/wells/${this.state.well._id}`}
                    >Well - {this.state.well.name}</Link> 
                    <Link 
                      className="list-group-item list-group-item-action bg-secondary text-light"
                      to="/organism-samples"
                    >Organism Sample List</Link>                    
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-primary text-light"
                        to={`/organism-samples/${this.state.sample._id}/edit`}
                      >Edit Organism Sample</Link>
                    ) : null }
                  </ul>
                </div>
              ) : null }  
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OrganismSampleProfile;