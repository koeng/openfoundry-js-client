import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class WellDelete extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      well: {}
    };
    this.getWell = this.getWell.bind(this);
    this.removeWell = this.removeWell.bind(this);
  }

  getWell() {
    const wellId = this.props.match.params.wellId;
    axios.get(`https://api.biohacking.services/wells/${wellId}`)
    .then(res => {     
      this.setState({
        well: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }

  removeWell() {
    const wellId = this.props.match.params.wellId;

    let config = {
      'headers': {
        'authorization': `Bearer ${Auth.getToken()}`,
        //'Content-Type': 'application/x-www-form-urlencoded'
      },
      'json': true
    };  
    axios.post(`https://api.biohacking.services/wells/${wellId}/remove`, wellId, config)
    .then(res => {       
      this.setState({
        redirect: true
      });
    })
    .catch(error => {
      console.error(error);
    });      
      
  }

  componentDidMount() {
    this.getWell();
  }

  render() {
    const wellId = this.props.match.params.wellId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/wells`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.well).length > 0) ? (
                  <h4 className="card-title mb-0">Delete {this.state.well.name}?</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Well...</h4>
                )}  
              </div>
              {(Object.keys(this.state.well).length > 0) ? (       
                <div className="card-body">
                  <p className="card-text">
                    Are you sure you want to Delete {this.state.well.name}? This action cannot be undone.
                  </p>
                </div>
              ) : null }  
              <div className="form-group text-center">
                <div className="btn-group" role="group">
                  <Link to={`/wells/${wellId}/edit`} className="btn btn-secondary mt-3">Back</Link>
                  <button 
                    className="btn btn-danger mt-3"
                    onClick={this.removeWell}
                  >
                    Delete {this.state.well.name}
                  </button>
                </div>
              </div>               
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default WellDelete;