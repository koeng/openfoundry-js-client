import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class WellList extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      wells: []
    };
    this.getAllWells = this.getAllWells.bind(this);
  }

  getAllWells() {
    axios.get(`https://api.biohacking.services/wells`)
    .then(res => {
      //console.log(res.data);
      this.setState({
        wells: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  componentDidMount() {
    this.getAllWells();
  }

  render() {
    const wells = this.state.wells.map((well, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/wells/${well._id}`}
        >
          {well.name}
        </Link>
      )
    });    
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">
            
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Wells</h4>
              </div>
              {(this.state.wells.length > 0) ? (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      Here is the list of well nodes:
                    </p>
                  </div>                  
                  <ul className="list-group list-group-flush">
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-success text-light"
                        to="/wells/new"
                      >Add New Well</Link>  
                    ) : null }
                    { wells }
                  </ul>
                </div>  
              ) : (
                <div>
                  <div className="card-body">
                    <p className="card-text">
                      There are currently no well nodes.
                    </p>
                  </div>
                  <ul className="list-group list-group-flush">
                    { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                      <Link 
                        className="list-group-item list-group-item-action bg-success text-light"
                        to="/wells/new"
                      >Add New Well</Link>  
                    ) : null }
                  </ul>                  
                </div>  
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WellList;