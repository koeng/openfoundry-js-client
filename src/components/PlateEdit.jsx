import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Auth from '../modules/Auth';
import axios from 'axios';

class PlateEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      plateId: "",
      plate: {
        parentId: "",
        name: "",
        description: "",
        storageTemperature: 0,
        potentialLocation: [],
        parentLocation: ""
      }
    };
    this.getPlate = this.getPlate.bind(this);
    this.updatePlate = this.updatePlate.bind(this);
    this.submitEditPlate = this.submitEditPlate.bind(this);
    this.handleNewPlateFormSubmit = this.handleNewPlateFormSubmit.bind(this);
  }

  getPlate() {
    const plateId = this.props.match.params.plateId;
    axios.get(`https://api.biohacking.services/plates/${plateId}`)
    .then(res => {     
      this.setState({
        plate: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });       
  }


  updatePlate(e) {
    const field = e.target.name;
    let plate = this.state.plate;
    plate[field] = e.target.value;
    this.setState({
      plate
    });    
  }

  submitEditPlate(plate) {
    const plateId = this.props.match.params.plateId;
    if(plate.name.length > 0){
      let config = {
        'headers': {
          'authorization': `Bearer ${Auth.getToken()}`,
          //'Content-Type': 'application/x-www-form-urlencoded'
        },
        'json': true
      };  
      axios.post(`https://api.biohacking.services/plates/${plateId}/edit`, plate, config)
      .then(res => {       
        this.setState({
          redirect: true
        });
      })
      .catch(error => {
        console.error(error);
        this.setState({ plate });
      });      
    }  
  }

  handleNewPlateFormSubmit(e) {
    e.preventDefault();
    let plate = this.state.plate;
    this.submitEditPlate(plate);
  }

  componentDidMount() {
    this.getPlate();
  }

  render() {
    const plateId = this.props.match.params.plateId;
    if (this.state.redirect === true) {
      return ( <Redirect to={`/plates/${plateId}`}/> )
    }
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-9 col-lg-7 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                {(Object.keys(this.state.plate).length > 0) ? (
                  <h4 className="card-title mb-0">Edit {this.state.plate.name}</h4>
                ) : (
                  <h4 className="card-title mb-0">Loading Plate...</h4>
                )}  
              </div>
              {(Object.keys(this.state.plate).length > 0) ? (
                <div>
                  <div className="card-body">
                    <form onSubmit={this.handleNewPlateFormSubmit}>
                      <div className="form-group">
                        <label htmlFor="name">Name</label>
                        <input 
                          name="name"
                          className="form-control"
                          value={this.state.plate.name}
                          onChange={this.updatePlate}
                          placeholder="Plate Name"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <input 
                          name="description"
                          className="form-control"
                          value={this.state.plate.description}
                          onChange={this.updatePlate}
                          placeholder="A short description of the Plate."
                        />
                      </div>

                      <div className="form-group">
                        <label htmlFor="name">Storage Temperature (C)</label>
                        <input 
                          name="storageTemperature"
                          type="number"
                          className="form-control"
                          value={this.state.plate.storageTemperature}
                          onChange={this.updatePlate}
                          min="-80"
                          step="1"
                        />
                      </div>

                      <div className="form-group text-center">
                        <div className="btn-group" role="group">
                          <Link to={`/plates/${plateId}`} className="btn btn-secondary mt-3">Back</Link>
                          <Link to={`/plates/${plateId}/delete`} className="btn btn-danger mt-3">Delete</Link>
                          <button type="submit" className="btn btn-success mt-3">Save Changes</button>
                        </div>
                      </div>                    
                                         
                    </form>    

                  </div>
                </div>
              ) : null }  
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default PlateEdit;