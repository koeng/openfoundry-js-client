import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class TransformPlate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ggs: []
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onGGToggle = this.onGGToggle.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    alert('ToDo: Handle Submit Data To Executor');
  }

  onGGToggle(e) {
    let ggs = this.state.ggs;
    let ggsIndex = ggs.indexOf(e.target.value);
    if (ggsIndex > -1) {
      ggs.splice(ggsIndex, 1);
    } else {
      ggs.push(e.target.value);
    }
    this.setState({
      ggs
    });
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Transform &amp; Plate</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
           
                    <form onSubmit={this.onFormSubmit}>
                      <h4 className="card-title">
                        Choose Which GG To Transform:
                      </h4>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="gg1" 
                          id="gg1"
                          onChange={this.onGGToggle}
                        />
                        <label className="form-check-label" htmlFor="gg1">
                          GG.1
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="gg2" 
                          id="gg2"
                          onChange={this.onGGToggle}
                        />
                        <label className="form-check-label" htmlFor="gg2">
                          GG.2
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="gg3" 
                          id="gg3"
                          onChange={this.onGGToggle}
                        />
                        <label className="form-check-label" htmlFor="gg3">
                          GG.3
                        </label>
                      </div>
                      
                      <h4 className="card-title mt-3">
                        Choose Which GG To Retransform:
                      </h4>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="gg4" 
                          id="gg4"
                          onChange={this.onGGToggle}
                        />
                        <label className="form-check-label" htmlFor="gg4">
                          GG.4
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="gg5" 
                          id="gg5"
                          onChange={this.onGGToggle}
                        />
                        <label className="form-check-label" htmlFor="gg5">
                          GG.5
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="gg6" 
                          id="gg6"
                          onChange={this.onGGToggle}
                        />
                        <label className="form-check-label" htmlFor="gg6">
                          GG.6
                        </label>
                      </div>

                      <div className="form-group mt-3">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <Link to="/transform" className="btn btn-block btn-light">Transform Menu</Link>
                          <button type="submit" className="btn btn-block btn-success">Confirm</button>
                        </div>  
                      </div>
                    </form> 
                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default TransformPlate;
