import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class BuildGenes extends Component {

  constructor(props) {
    super(props);
    this.state = {
      confirmVisible: false,
      geneCount: 1
    };
    this.onChangeGeneCount = this.onChangeGeneCount.bind(this);
    this.onInputFormSubmit = this.onInputFormSubmit.bind(this);
    this.onConfirmSubmit = this.onConfirmSubmit.bind(this);
    this.toggleConfirmVisible = this.toggleConfirmVisible.bind(this);
  }

  onChangeGeneCount(e) {
    let geneCount = e.target.value;
    this.setState({ geneCount });
  }

  onInputFormSubmit(e) {
    e.preventDefault();
    this.toggleConfirmVisible();
  }

  onConfirmSubmit(e) {
    e.preventDefault();
    alert('ToDo: Handle Submit Data To Executor');
    this.toggleConfirmVisible();
  }

  toggleConfirmVisible() {
    this.setState({
      confirmVisible: !this.state.confirmVisible
    });
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Build Genes</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
                  {(!this.state.confirmVisible) ? (
                    <form onSubmit={this.onInputFormSubmit}>
                      <h4 className="card-title">
                        Please Enter Number Of Genes:
                      </h4>
                      <div className="form-group">
                        <label htmlFor="geneCount">Gene Count</label>
                        <input
                          type="number"
                          id="geneCount"
                          className="form-control"
                          name="geneCount"
                          min="1"
                          max="100"
                          step="1"
                          value={this.state.geneCount}
                          onChange={this.onChangeGeneCount}
                        />
                      </div>
                      <div className="form-group">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <Link to="/build" className="btn btn-block btn-light">Build Menu</Link>
                          <button type="submit" className="btn btn-block btn-success">Next</button>
                        </div>  
                      </div>
                    </form>
                  ) : (
                    <form onSubmit={this.onConfirmSubmit}>
                      <h4 className="card-title">
                        Genes Being Built:
                      </h4>
                      <ul style={{'listStyle': 'none', 'padding': '0'}}>
                        <li>Part 1</li>
                        <li>Part 2</li>
                        <li>Part 3</li>
                        <li>Part 4</li>
                        <li>Part 5</li>
                      </ul>
                      <div className="form-group">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <button onClick={this.toggleConfirmVisible} className="btn btn-block btn-light">Cancel</button>
                          <button type="submit" className="btn btn-block btn-success">Confirm</button>
                        </div>
                      </div>
                    </form>
                  )}

                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default BuildGenes;
