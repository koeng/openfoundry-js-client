import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import shortid from 'shortid';

class FreezerList extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      freezers: []
    };
    this.getAllFreezers = this.getAllFreezers.bind(this);
  }

  getAllFreezers() {
    axios.get(`https://api.biohacking.services/freezers`)
    .then(res => {
      //console.log(res.data);
      this.setState({
        freezers: res.data.data
      });        
    })
    .catch(error => {
        console.error(error);        
    });    
  }

  componentDidMount() {
    this.getAllFreezers();
  }

  render() {
    const freezers = this.state.freezers.map((freezer, index) => {
      return (
        <Link 
          key={shortid.generate()}
          className="list-group-item list-group-item-action"
          to={`/freezers/${freezer._id}`}
        >
          {freezer.name}
        </Link>
      )
    });    
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7">
            
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Freezers</h4>
              </div>
              <div className="card-body">
                <p className="card-text">
                  Here is the list of freezer nodes:
                </p>
              </div>
              <ul className="list-group list-group-flush">
                { (this.props.isLoggedIn && this.props.currentUser.isAdmin) ? (
                  <Link 
                    className="list-group-item list-group-item-action bg-success text-light"
                    to="/freezers/new"
                  >Add New Freezer</Link>  
                ) : null }
                { freezers }
              </ul>
            </div>

          </div>
        </div>
      </div>
    );
  }
}

export default FreezerList;