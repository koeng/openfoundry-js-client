import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class BuildSpecificGenes extends Component {

  constructor(props) {
    super(props);
    this.state = {
      confirmVisible: false,
      parts: []
    };
    this.onInputFormSubmit = this.onInputFormSubmit.bind(this);
    this.onConfirmSubmit = this.onConfirmSubmit.bind(this);
    this.toggleConfirmVisible = this.toggleConfirmVisible.bind(this);
    this.onPartToggle = this.onPartToggle.bind(this);
  }

  onInputFormSubmit(e) {
    e.preventDefault();
    this.toggleConfirmVisible();
  }

  onConfirmSubmit(e) {
    e.preventDefault();
    alert('ToDo: Handle Submit Data To Executor');
    this.toggleConfirmVisible();
  }

  toggleConfirmVisible() {
    this.setState({
      confirmVisible: !this.state.confirmVisible
    });
  }

  onPartToggle(e) {
    let parts = this.state.parts;
    let partIndex = parts.indexOf(e.target.value);
    if (partIndex > -1) {
      parts.splice(partIndex, 1);
    } else {
      parts.push(e.target.value);
    }
    this.setState({
      parts
    });
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Build Specific Genes</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
                  {(!this.state.confirmVisible) ? (
                    <form onSubmit={this.onInputFormSubmit}>
                      <h4 className="card-title">
                        Please Select Genes To Be Built:
                      </h4>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="part1" 
                          id="part1"
                          onChange={this.onPartToggle}
                        />
                        <label className="form-check-label" htmlFor="part1">
                          Part 1
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="part2" 
                          id="part2"
                          onChange={this.onPartToggle}
                        />
                        <label className="form-check-label" htmlFor="part2">
                          Part 2
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="part3" 
                          id="part3"
                          onChange={this.onPartToggle}
                        />
                        <label className="form-check-label" htmlFor="part3">
                          Part 3
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="part4" 
                          id="part4"
                          onChange={this.onPartToggle}
                        />
                        <label className="form-check-label" htmlFor="part4">
                          Part 4
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="part5" 
                          id="part5"
                          onChange={this.onPartToggle}
                        />
                        <label className="form-check-label" htmlFor="part5">
                          Part 5
                        </label>
                      </div>

                      <div className="form-group mt-3">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <Link to="/build" className="btn btn-block btn-light">Build Menu</Link>
                          <button type="submit" className="btn btn-block btn-success">Next</button>
                        </div>  
                      </div>
                    </form>
                  ) : (
                    <form onSubmit={this.onConfirmSubmit}>
                      <h4 className="card-title">
                        Genes Being Built:
                      </h4>
                      <ul style={{'listStyle': 'none', 'padding': '0'}}>
                        {(this.state.parts.indexOf('part1') > -1) ? ( <li>Part 1</li> ) : null }
                        {(this.state.parts.indexOf('part2') > -1) ? ( <li>Part 2</li> ) : null }
                        {(this.state.parts.indexOf('part3') > -1) ? ( <li>Part 3</li> ) : null }
                        {(this.state.parts.indexOf('part4') > -1) ? ( <li>Part 4</li> ) : null }
                        {(this.state.parts.indexOf('part5') > -1) ? ( <li>Part 5</li> ) : null }
                      </ul>
                      <div className="form-group text-center">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <button onClick={this.toggleConfirmVisible} className="btn btn-block btn-light">Cancel</button>
                          <button type="submit" className="btn btn-block btn-success">Confirm</button>
                        </div>
                      </div>
                    </form>
                  )}

                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default BuildSpecificGenes;
