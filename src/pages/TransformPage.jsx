import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class TransformPage extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Transform Menu</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
                  <p className="card-text">
                    Please Select From The Transform Options Below:
                  </p>
                  <div className="btn-group-vertical" style={{'width': '100%'}}>
                    <Link to="/" className="btn btn-block btn-light">Main Menu</Link>
                    <Link to="/transform/plate" className="btn btn-block btn-success">Transform &amp; Plate</Link>
                    <Link to="/transform/pick" className="btn btn-block btn-success">Pick</Link>
                    <Link to="/transform/consolidate" className="btn btn-block btn-success">Consolidate Plates</Link>
                  </div>
                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default TransformPage;
