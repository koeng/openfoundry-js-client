import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class BuildPage extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Build Menu</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
                  <p className="card-text">
                    Please Select From The Build Options Below:
                  </p>
                  <div className="btn-group-vertical" style={{'width': '100%'}}>
                    <Link to="/" className="btn btn-block btn-light">Main Menu</Link>
                    <Link to="/build/genes" className="btn btn-block btn-info">Build Genes</Link>
                    <Link to="/build/specific-genes" className="btn btn-block btn-info">Build Specific Genes</Link>
                  </div>
                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default BuildPage;
