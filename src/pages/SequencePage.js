import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SequencePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      confirmVisible: false,
      plates: [],
      partialPlates: []
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onPlateToggle = this.onPlateToggle.bind(this);
    this.onPartialUpdate = this.onPartialUpdate.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    alert('ToDo: Handle Submit Data To Executor');
  }

  onPlateToggle(e) {
    let plates = this.state.plates;
    let plateIndex = plates.indexOf(e.target.value);
    if (plateIndex > -1) {
      plates.splice(plateIndex, 1);
    } else {
      plates.push(e.target.value);
    }
    this.setState({
      plates
    });
  }

  onPartialUpdate(e) {

  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row mt-3">
          <div className="col col-md-7 col-lg-5 ml-md-auto mr-md-auto text-center">
            <div className="card">
              <div className="card-header bg-dark text-light">
                <h4 className="card-title mb-0">Sequence</h4>
              </div>
              { this.props.isLoggedIn ? (
                <div className="card-body">
           
                    <form onSubmit={this.onFormSubmit}>
                      <h4 className="card-title">
                        Choose Plates To Sequence:
                      </h4>

                      <h5 className="card-title">
                        Unsequenced Plates:
                      </h5>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate1" 
                          id="plate1"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="plate1">
                          Plate 1
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate2" 
                          id="plate2"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="plate2">
                          Plate 2
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate3" 
                          id="plate3"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="plate3">
                          Plate 3
                        </label>
                      </div>

                      <h5 className="card-title mt-3">
                        Partially Sequenced Plates:
                      </h5>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate4-entire-plate"
                          name="plate4-entire-plate"
                          id="plate4-entire-plate"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="plate4-entire-plate">
                          Plate 4 - Entire Plate
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate4-failed-wells"
                          name="plate4-failed-wells"
                          id="plate4-failed-wells"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="plate4-failed-wells">
                          Plate 4 - Only Failed Wells
                        </label>
                      </div>

                      <h5 className="card-title mt-3">
                        Resequence Plates:
                      </h5>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate5" 
                          id="plate5"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="plate5">
                          Plate 5
                        </label>
                      </div>

                      <div className="form-check">
                        <input 
                          className="form-check-input" 
                          type="checkbox" 
                          value="plate6" 
                          id="plate6"
                          onChange={this.onPlateToggle}
                        />
                        <label className="form-check-label" htmlFor="plate6">
                          Plate 6
                        </label>
                      </div>                      

                      <div className="form-group mt-3">
                        <div className="btn-group-vertical" style={{'width': '100%'}}>
                          <Link to="/" className="btn btn-block btn-light">Main Menu</Link>
                          <button type="submit" className="btn btn-block btn-success">Confirm</button>
                        </div>  
                      </div>
                    </form> 
                </div>
              ) : (
                <div className="card-body">
                  <p className="card-text">
                    Welcome to OpenFoundry! Please <Link to="/login">Login</Link> to continue.
                  </p>
                </div>                
              )}  
              
                { !this.props.isLoggedIn ? (
                  <ul className="list-group list-group-flush">
                    <Link 
                      className="list-group-item list-group-item-action bg-success text-light"
                      to="/login"
                    >Login</Link>
                    <Link 
                      className="list-group-item list-group-item-action bg-primary text-light"
                      to="/signup"
                    >Sign Up</Link>
                  </ul>
                ) : null }

            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default SequencePage;
